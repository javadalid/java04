package deloitte.academy.lesson01.run;

import deloitte.academy.lesson01.entity.Empleado;
import deloitte.academy.lesson01.entity.Habitacion;
import deloitte.academy.lesson01.entity.HuespedFrecuente;
import deloitte.academy.lesson01.entity.HuespedNormal;
import deloitte.academy.lesson01.entity.TiposHab;
import deloitte.academy.lesson01.logic.Operations;

/**
 * Clase main que realiza todas las operaciones del hotel.
 * 
 * @author Javier Adalid
 *
 */
public class Ejecutar {

	public static void main(String[] args) {

		// Se crean tres huespedes, un normal, un frecuente y un empleado
		Empleado empleado = new Empleado(1, "Empleado de prueba", "Direccion de prueba");
		HuespedFrecuente huespedFrecuente = new HuespedFrecuente(1, "Huesped frecuente de prueba", 203);
		HuespedNormal huespedNormal = new HuespedNormal(1, "Huesped normal de prueba");

		// Se dan de alta las habitaciones
		new Habitacion(100, TiposHab.ESTANDAR);
		new Habitacion(101, TiposHab.JRSUITE);
		new Habitacion(102, TiposHab.SUITE);
		new Habitacion(103, TiposHab.ESTANDAR);

		// check in de habitacion 100
		Operations.checkIn(100, empleado);

		// check in fallido por ser la misma habitacion
		Operations.checkIn(100, empleado);

		// check in de habitacion 101
		Operations.checkIn(101, huespedFrecuente);

		// check in de habitacion 102
		Operations.checkIn(102, huespedNormal);

		// check in fallido por ser una habitacion inexistente
		Operations.checkIn(105, huespedNormal);

		// Muestra de habitaciones disponibles
		Operations.verHabitacionesDisponibles();

		// Checkout de la habitacion 100
		Operations.checkOut(100, 300.90);

		// Checkout de la habitacion 101
		Operations.checkOut(101, 300.90);

		// Checkout de la habitacion 102
		Operations.checkOut(102, 300.90);

		// Checkout fallido
		Operations.checkOut(103, 300.90);

		// Muestra de habitaciones disponibles
		Operations.verHabitacionesDisponibles();

	}

}
