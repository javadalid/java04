package deloitte.academy.lesson01.entity;
/**
 * Uso de un enum para los tipos de habitaciones
 * @author Javier Adalid
 *
 */
public enum TiposHab {
	SUITE, ESTANDAR, JRSUITE;
}
