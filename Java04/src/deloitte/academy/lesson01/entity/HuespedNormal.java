package deloitte.academy.lesson01.entity;

/**
 * Clase Huesped Normal. Permite su creaci�n y sobrescribe el m�todo cobrar.
 * 
 * @author Javier Adalid
 *
 */
public class HuespedNormal extends Huesped {
	/**
	 * Constructor de un hu�sped normal.
	 * 
	 * @param id:     el id del huesped type int.
	 * @param nombre: el nombre del hu�sped type string.
	 */
	public HuespedNormal(int id, String nombre) {
		super(id, nombre);
	}

	/**
	 * Sobreescritura del m�todo de cobro del hu�sped.
	 */
	@Override
	public double cobrar() {
		return this.getTotal();
	}

}
