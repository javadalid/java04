package deloitte.academy.lesson01.entity;

/**
 * Clase que permite crear y cobrar a un Hu�sped Frecuente.
 * 
 * @author Javier Adalid
 *
 */
public class HuespedFrecuente extends Huesped {

	private int numTarjeta;

	/**
	 * Constructor de un hu�sped frecuente
	 * 
	 * @param id:         type int
	 * @param nombre:     nombre del hu�sped, type string.
	 * @param numTarjeta: n�mero de tarjeta de hu�sped frecuente, type int.
	 */
	public HuespedFrecuente(int id, String nombre, int numTarjeta) {
		super(id, nombre);
		this.numTarjeta = numTarjeta;
	}

	/**
	 * Sobreescritura del m�todo cobrar, con el 80% de descuento.
	 */
	@Override
	public double cobrar() {
		return this.getTotal() * .80;
	}

}