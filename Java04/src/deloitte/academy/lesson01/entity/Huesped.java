package deloitte.academy.lesson01.entity;

/**
 * Clase abstracta huesped. Contiene el m�todo cobrar, que ser� implementado en
 * las clases que hereden de �sta.
 * 
 * @author Javier Adalid
 *
 */
public abstract class Huesped {
	protected int id;
	private String nombre;
	private double total;

	/**
	 * Constructor de un hu�sped.
	 * 
	 * @param id:     el id del huesped, identificador. Type int.
	 * @param nombre: nombre del huesped. Type string.
	 */
	public Huesped(int id, String nombre) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.total = 0;
	}

	/**
	 * Obtiene el total, lo devuelve como double.
	 */
	public double getTotal() {
		return total;
	}

	/**
	 * Permite modificar el total
	 * 
	 * @param total, tipo double
	 */
	public void setTotal(double total) {
		this.total = total;
	}

	/**
	 * Obtiene el id
	 * 
	 * @return id como int
	 */
	public int getId() {
		return id;
	}

	/**
	 * Permite modificar el id
	 * 
	 * @param id, como integer.
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Obtiene el nombre.
	 * 
	 * @return nombre como string
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Permite modificar el nombre
	 * 
	 * @param nombre, type string.
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * M�todo abstracto de cobro. Tiene que ser implementado en todas las clases
	 * hijo.
	 * 
	 * @return
	 */
	public abstract double cobrar();

}
