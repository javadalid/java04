package deloitte.academy.lesson01.entity;

/**
 * Clase que permite crear un Empleado (Hu�sped).
 * 
 * @author Javier Adalid
 *
 */
public class Empleado extends Huesped {
	private String direccion;

	/**
	 * Constructor de un Empleado
	 * 
	 * @param id:        type int
	 * @param nombre:    nombre del empleado type string.
	 * @param direccion: direccion del empleado type string
	 */
	public Empleado(int id, String nombre, String direccion) {
		super(id, nombre);
		this.direccion = direccion;
	}

	/**
	 * Sobreescritura del m�todo cobrar, con el descuento del 70%.
	 */
	@Override
	public double cobrar() {
		return this.getTotal() * .70;
	}
}
