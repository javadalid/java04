package deloitte.academy.lesson01.entity;

import java.util.ArrayList;

/**
 * Clase que permite crear una habitacion
 * 
 * @author Javier Adalid
 *
 */
public class Habitacion {

	// Declaración de atributos
	private int numeroHabitacion;
	private Huesped huesped;
	private boolean disponible;
	private TiposHab tipo;

	// ArrayList que guarda las habitaciones creadas
	public static ArrayList<Habitacion> habitaciones = new ArrayList<Habitacion>();

	/**
	 * Constructor de habitaciones
	 * 
	 * @param numeroHabitacion type int
	 * @param tipo, tipo-habitacion type enum
	 */
	public Habitacion(int numeroHabitacion, TiposHab tipo) {
		super();
		this.numeroHabitacion = numeroHabitacion;
		this.huesped = null;
		this.tipo = tipo;
		this.disponible = true;
		habitaciones.add(this);
	}

	/**
	 * 
	 * Getters y setters de todos los atributos de la clase Habitacion
	 */
	
	public int getNumeroHabitacion() {
		return numeroHabitacion;
	}

	public void setNumeroHabitacion(int numeroHabitacion) {
		this.numeroHabitacion = numeroHabitacion;
	}

	public Huesped getHuesped() {
		return huesped;
	}

	public void setHuesped(Huesped huesped) {
		this.huesped = huesped;
	}

	public boolean isDisponible() {
		return disponible;
	}

	public void setDisponible(boolean disponible) {
		this.disponible = disponible;
	}

	public TiposHab getTipo() {
		return tipo;
	}

	public void setTipo(TiposHab tipo) {
		this.tipo = tipo;
	}
}
